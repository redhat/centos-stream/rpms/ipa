From 934d4a291d44a40b5ea006aa1f09afa8e4a985fc Mon Sep 17 00:00:00 2001
From: Rob Crittenden <rcritten@redhat.com>
Date: Mon, 2 Dec 2024 10:27:15 -0500
Subject: [PATCH] Pass all pkiuser groups as suplementary when validating an
 HSM

We were doing a "best effort" when validating the HSM token is
visible with a valid PIN when it came to groups. A specific
workaround was added for softhsm2 but this didn't carry over
to other HSMs that may have group-specific read/write access.

Use the new capability in ipaplatform.constants.py::Group to be
able to use generate a valid entry from a group GID. Pair this
with os.getgrouplist() and all groups will be passed correctly
via ipautil.run().

Fixes: https://pagure.io/freeipa/issue/9709

Signed-off-by: Rob Crittenden <rcritten@redhat.com>
Reviewed-By: Alexander Bokovoy <abokovoy@redhat.com>
Reviewed-By: Florence Blanc-Renaud <frenaud@redhat.com>
---
 ipaserver/install/ca.py | 12 ++++--------
 1 file changed, 4 insertions(+), 8 deletions(-)

diff --git a/ipaserver/install/ca.py b/ipaserver/install/ca.py
index 520e3fc5de1084e7c22c0cf7eaa86e1d3c421373..2959aceed5cd2fd4851457eaa4aeac3c0905d27d 100644
--- a/ipaserver/install/ca.py
+++ b/ipaserver/install/ca.py
@@ -211,11 +211,7 @@ def hsm_validator(token_name, token_library, token_password):
         )
     pkiuser = constants.PKI_USER
     pkigroup = constants.PKI_GROUP
-    if 'libsofthsm' in token_library:
-        import grp
-        group = grp.getgrnam(constants.ODS_GROUP)
-        if str(constants.PKI_USER) in group.gr_mem:
-            pkigroup = constants.ODS_GROUP
+    group_list = os.getgrouplist(pkiuser, pkigroup.gid)
     with certdb.NSSDatabase() as tempnssdb:
         tempnssdb.create_db(user=str(pkiuser), group=str(pkigroup))
         # Try adding the token library to the temporary database in
@@ -231,7 +227,7 @@ def hsm_validator(token_name, token_library, token_password):
         # It may fail if p11-kit has already registered the library, that's
         # ok.
         ipautil.run(command, stdin='\n', cwd=tempnssdb.secdir,
-                    runas=pkiuser, suplementary_groups=[pkigroup],
+                    runas=pkiuser, suplementary_groups=group_list,
                     raiseonerr=False)
 
         command = [
@@ -242,7 +238,7 @@ def hsm_validator(token_name, token_library, token_password):
         ]
         lines = ipautil.run(
             command, cwd=tempnssdb.secdir, capture_output=True,
-            runas=pkiuser, suplementary_groups=[pkigroup]).output
+            runas=pkiuser, suplementary_groups=group_list).output
         found = False
         token_line = f'token: {token_name}'
         for line in lines.split('\n'):
@@ -265,7 +261,7 @@ def hsm_validator(token_name, token_library, token_password):
         ]
         result = ipautil.run(args, cwd=tempnssdb.secdir,
                              runas=pkiuser,
-                             suplementary_groups=[pkigroup],
+                             suplementary_groups=group_list,
                              capture_error=True, raiseonerr=False)
         if result.returncode != 0 and len(result.error_output):
             if 'SEC_ERROR_BAD_PASSWORD' in result.error_output:
-- 
2.47.1

